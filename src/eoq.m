
function Qx = eoq(D, h , A, c)

  Q = [15 : 1 : 500];


  # calculate graphs
  for q = Q
    HC(q-14) = h * q / (2 * D);
    OC(q-14) = A / q;
    PC(q-14) = c;
    Y (q-14) = h * q / (2 * D) + A / q + c;
  endfor

  # calculate Q*
  Qx = sqrt(2 * A * D / h)
  Qy = h * Qx / (2 * D) + A / Qx + c;


  figure(1);

  xlabel("Order quantity (Q)");
  ylabel ("Cost/unit");


  hold on;
  h(1) = plot(Q, Y, "1");
  h(2) = plot(Q, HC, "2");
  h(3) = plot(Q, OC, "3");
  h(4) = plot(Q, PC, "4");
  h(5) = plot([Qx, Qx], [0, Qy], "0");
  set(h, "linewidth", 2);
  ## text(Qx-14, -1.8, strcat("Q*=", mat2str(round(Qx))))
  text(Qx-0.1, Qy+0.2, strcat("Q*=", mat2str(round(Qx))))

  legend("Y(Q)", "Holding Costs ", "Ordering Costs", "Production Costs", "EOQ = min(Y(Q))")


  ## plot([0, Qx], [Qy, Qy], "0")
  ## text(-15, Qy, "Q*")


  print("eoq.png", "-color");
  pause;

endfunction

