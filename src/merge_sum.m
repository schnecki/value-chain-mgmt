function val = merge_sum(list)
  list = sort(list);
  val = list(1,:);
  val(1,3) = 1;
  for i = [2:length(list)]
    mv = length(val(:,1));
    if val(mv,1) == list(i,1)
      m = length(val(:,1));
      val(mv,2) += list(i,2);
      val(mv,3) += 1;
    else
      m = length(val(:,1));
      val(m+1,1) = list(i,1);
      val(m+1,2) = list(i,2);
      val(m+1,3) = 1;
    endif
  endfor

endfunction
