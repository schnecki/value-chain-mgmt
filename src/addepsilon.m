function g = addepsilon(sigma, data)
  for i=1 : rows(data)
    y = data(i,2);
    g(i,1) = data(i,1);
    g(i,2) = gauss_rand(1,1,y,sigma);
  endfor
endfunction
