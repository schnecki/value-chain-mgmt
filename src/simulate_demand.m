
function Dem = simulate_demand(D,periods,simFun,sigmaD)

  ## # constant demand model
  ## # --------------------------------------------------
  ## sigmaD = 1                      # sigma to generate demand simulation items
  ## simFun = @(x)[D/periods]        # model for data generation
  legendLoc = "southeast";
  xAxisLabel = "Period";
  yAxisLabel = "Demand";
  ## # --------------------------------------------------


  # normal distributed demand model (seasonal demand)
  # --------------------------------------------------
  ## periods = 52                      # nr. of simulation demand points
  ## sigmaD = 4                        # sigma to generate demand simulation items
  ## mu = periods/2
  ## sigma = 10
  ## simFun = @(x)[D * (1/(sigma * sqrt(2 * pi)) * e^(- (x-mu)^2/(2*sigma^2)))]
  ## legendLoc = "northeast"
  ## xAxisLabel = "Month"
  ## yAxisLabel = "Demand"
  # --------------------------------------------------

  ## simFun = @(x)[sin(periods * x)+1] # model for data generation

  ## model of demand
  for i = [1:periods]
    M(i) = simFun(i);
  endfor

  ## simulation of demand
  SD = addepsilon(sigmaD, [1:periods; M]');

  for x = [1:periods]
    if SD(x,2) < 0
      SD(x,2) = 0;
    endif
  endfor

  Dem = SD(:,2)';

  figure(2)
  clf;
  hold on;
  ## plot([1:periods], M, "+");

  h(1) = plot([1:periods], M, "1" );
  h(2) = plot(SD(:,1), SD(:,2), "+");
  set(h, "linewidth", 2);
  legend("Modelled demand ", "Simulated demand ", "location", legendLoc)
  xlabel(xAxisLabel);
  ylabel(yAxisLabel);
  yAxisMax = max(SD(:,2)) + 5;
  axis([1 periods 0 yAxisMax])

  print("simulated_demand.png", "-color");

  pause

endfunction
