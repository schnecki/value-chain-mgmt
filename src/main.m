# setup
graphics_toolkit("gnuplot");

# config

## D_periods = [150, 159, 173, 195, 207]; # comment after testing
## D = sum(D_periods) # demand per year

## BASIC VARIABLES

D = 500                         # demand per year (may be computed automatically)
h = 7.50                        # holding cost per unit per year
A = 50                          # ordering cost per order per year
c = 1.00                        # production cost per unit
oh_inventory = 100              # oh at start

## SIMULATION OF DEMAND VARIABLES

# constant demand

periods=52                       # number of demand point per year
sigmaD = 2                       # sigma to generate demand simulation items
simFun = @(x)[D/periods]         # model for data generation

# seasonal demand function

## periods=52                       # number of demand point per year
## sigmaD = 2                       # sigma to generate demand simulation items
## sigma = 10                       # variance of season
## mu = periods/2                   # center of season
## simFun = @(x)[D * (1/(sigma * sqrt(2 * pi)) * e^(- (x-mu)^2/(2*sigma^2)))]

## uncomment to enable automatic calculation of D
## D = 0;
## for p = [1:periods]
##   D += simFun(p);
## endfor

## SERVICE LEVEL VARIABLES

service_level = 0.0        # desired service level [0,1)
sigmas = [23.94, 23.94]      # standard deviation of demand (length = lead time)


## Delivery Problem
delivery_problem = 0.40         # percentage of delivery problem
delivery_uncertainty_mean = 2   # mean of additional delivery lead time
delivery_uncertainty_sigma = 4  # distribution of additional lead time

## Quantity of received units
yield_uncertainty_sigma = 0     # uncertainty for delivery quantity


########################################################################
warning('off','all');           # turn of all warnings


EOQ = ceil(eoq(D,h,A,c))       # calculate EOQ with given data

D_avg = D / periods             # calculate average demand per period

D_periods = simulate_demand(D, periods, simFun, sigmaD); # create simulated the
                                                         # demand points

lead_time = length(sigmas);     # calculate lead time
sigmaL = sqrt(sum(sigmas.^2));  # calculate sigma over the lead time

if service_level > 0
  SS = norminv(service_level) * sigmaL # calculate service level
else
  SS = 0
endif
R = lead_time * D_avg + SS      # calculate Reorder point


## calculate reorder periods and corresponding inventory position
IP_current = oh_inventory;
i = 1;
j = 1;
k = 2;
l = 1;

## set start points of graph
IP(1,1) = 0;
IP(1,2) = IP_current;


for d = D_periods
  IP_current -= d;
  ## if IP_current <= 0
  ##   IP_current = 0;
  ## endif

  IP(k, 1) = l;
  IP(k++, 2) = IP_current;

  if IP_current <= R
    IP_current += EOQ;
    reorder_periods(i++) = j;
    IP(k, 1) = l;
    IP(k++, 2) = IP_current;
  endif
  j++;
  l++;
endfor


reorder_periods                 # print out reorder periods


IV_current = oh_inventory;
## delivery_periods = reorder_periods + lead_time;

m = 1;
for p = reorder_periods
  n = rand(1,1) < delivery_problem;
  if n == 1
    printf("Delivery problem for order in period number %d\n", p);
  endif
  delivery_periods(m, 1) = p + lead_time + abs(n * int32(gauss_rand(1,1,delivery_uncertainty_mean, delivery_uncertainty_sigma)));
  delivery_periods(m++, 2) = gauss_rand(1,1,EOQ,yield_uncertainty_sigma);
endfor

delivery_periods = merge_sum(delivery_periods) # print out delivery_periods


IV(1,1) = 0;
IV(1,2) = IV_current;
i = 1;
j = 1;
k = 2;
l = 1;
m = 1;
next_delivery_period = delivery_periods(1,1);
## delivery_periods = delivery_periods(2:end,:);

next_reorder_period = reorder_periods(1);
reorder_periods = reorder_periods(2:end);

IP_current = oh_inventory;
IP2(1,1) = 0;
IP2(1,2) = IP_current;

# calculate inventory
for d = D_periods
  IV_current -= d;
  IP_current -= d;

  IP2(m, 1) = l;
  IP2(m++, 2) = IP_current;
  IV(k, 1) = l;
  IV(k++, 2) = IV_current;

  if int32(next_reorder_period) == l
    IP_current += EOQ;
    IP2(m, 1) = l;
    IP2(m++, 2) = IP_current;
    next_reorder_period = reorder_periods(1);
    if length(reorder_periods) > 1
      reorder_periods = reorder_periods(2:end);
    endif
  endif

  if int32(next_delivery_period) == l

    ## fit IP to delivered amount
    IP_current += delivery_periods(1,2) - delivery_periods(1,3) * EOQ;
    IP2(m, 1) = l;
    IP2(m++, 2) = IP_current;


    IV_current += delivery_periods(1,2);
    IV(k, 1) = l;
    IV(k++, 2) = IV_current;

    if length(delivery_periods(:,1)) > 1
      delivery_periods = delivery_periods(2:end,:);
    endif
    next_delivery_period = delivery_periods(1,1);


  endif
  j++;
  l++;
endfor


figure(3);
clf;
xlabel("Time");
ylabel ("Inventory (Position)");

## points for reorder point
for x = [0:1:periods]
  reorder(x+1,1) = x;
  reorder(x+1,2) = R;
endfor

hold on;
## h(1) = plot(Q, Y, "1");
## h(2) = plot(Q, HC, "2");
## h(3) = plot(Q, OC, "3");
## h(4) = plot(Q, PC, "4");
## h(1) = plot(IP(:,1), IP(:,2), "1");
h(1) = plot(IP2(:,1), IP2(:,2), "1");
h(2) = plot(reorder(:,1), reorder(:,2)', "2");
h(3) = plot(IV(:,1), IV(:,2), "3");
set(h, "linewidth", 2);
## text(Qx-14, -1.8, strcat("Q*=", mat2str(round(Qx))))
## text(Qx-0.1, Qy+0.2, strcat("Q*=", mat2str(round(Qx))))

legend("Inventory Position ", "Reorder Point ", "Net Inventory ")


## plot([0, Qx], [Qy, Qy], "0")
## text(-15, Qy, "Q*")

print("ip.png", "-color");
pause;


