function r = gauss_rand(n, m, mu, sigma)
  % Generate random number based on inverse transform sampling method
  % n: number of rows
  % m: number of columns
  % mu: mean of distribution
  % sigma: standard deviation of distribution

  if nargin < 4
    mu = 0;
    sigma = 1;
  end
  r = sqrt(2) * sigma * erfinv(2 * rand(n, m) - 1) + mu;
end
