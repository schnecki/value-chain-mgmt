(TeX-add-style-hook
 "01_presentation"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("beamer" "t")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (TeX-run-style-hooks
    "latex2e"
    "presentation_setup"
    "beamer"
    "beamer10"
    "inputenc"
    "fontenc"
    "fixltx2e"
    "graphicx"
    "longtable"
    "float"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "marvosym"
    "wasysym"
    "amssymb"
    "hyperref"
    "threeparttable"
    "color"
    "colortbl"
    "appendixnumberbeamer")
   (LaTeX-add-labels
    "sec-1"
    "sec-3"
    "fig:example"
    "sec-5"
    "sec-7"
    "sec-10"
    "sec-11")
   (LaTeX-add-bibliographies
    "biblio")))

